<?php
if(!function_exists('generateBreadcrumb')){

function generateBreadcrumb()
{
  
  $ci = &get_instance();
  $ci->load->database();
  
  $module = $ci->uri->segment(1);
  $action = $ci->uri->segment(2);
  $breadcrumb=array();

            $ci->db->select('*');
            $ci->db->from('_menu');
            $ci->db->where('link',$module);
            $query =$ci->db->get();
            $row=$query->row();        
            $breadcrumb[]=$row->menu_caption;
            $breadcrumb[]=$row->link;

      $parent_id=$row->parent;
      while($parent_id != 0)
      { 

            $ci->db->select('*');
            $ci->db->from('_menu');
            $ci->db->where('id',$parent_id);
            $query1 =$ci->db->get();
            $row1=$query1->row();
            $breadcrumb[]=$row1->menu_caption;
            $breadcrumb[]=$row1->link;
            $parent_id=$row1->parent;

      }       
 switch ($action) {  
    case "add":
        $font_class="glyphicon glyphicon-plus";
        $lable="Add";
        break;
    case "view":
        $font_class="fa fa-eye";
        $lable="View";
        break; 
    case "edit":
        $font_class="fa fa-pencil";
        $lable="Edit";
        break;
    default:      
        $font_class="fa fa-table";
        $lable="List";
        break;
    }
 

  $link = '<div class="header-content">
                <h2><i class="'.$font_class.'"></i>'.$breadcrumb[0].'<span>'.$lable.'</span></h2>
              <div class="breadcrumb-wrapper hidden-xs">
              <span class="label">You are here:</span>
            <ol class="breadcrumb">
            <li>
                <i class="fa fa-home"></i>
                <a href="'.base_url('dashboard').'">Dashboard</a>
                <i class="fa fa-angle-right"></i>
            </li>';
       
    $arr=array_reverse($breadcrumb); 
    $arr1=count($arr);
    $i=0;  
     foreach($arr as $value)
     {
       if($i < ($arr1))
       {
        $lk=$arr[$i] == null ? '#' : base_url($arr[$i]);
       $link .='<li><a href='.$lk.'>'.$arr[$i+1].'<i class="fa fa-angle-right"></i>
            </li>';
        }
        else
        {
          break;
        }
         $i=$i+2;   
     }  

  $link .= '<li class="active">
                <a href="#">'.$lable.'</a>
                <i class="fa fa-angle-right"></i>
            </li>
            </ol>
             </div>
            </div>';
            
    return $link;
  }
  
}
 
?>