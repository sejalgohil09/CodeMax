<?php
if(!function_exists('errors')){

function errors()
{
  
  $ci = &get_instance();
   // Set title page
        $ci->smartyci->assign('title', 'ERROR 404');

        // Set CSS page
        $css_page = array(
            'error-page.css'
        );
        $ci->smartyci->assign('list_css_page',$css_page);

        // Set content page
        $ci->smartyci->assign('body', 'contents/errors/404.html');
                          
        // Set active menu
        $ci->smartyci->assign('active_pages', 'active');
        $ci->smartyci->assign('active_error', 'active');
        $ci->smartyci->assign('active_error_404', 'active');
        
        // Render view on main layout
        $ci->smartyci->display('contents/layout.html');
  
}
 }
?>