<?php if(!function_exists('pagination')){
 
  
    function pagination($base_url,$modelname,$selectfunname,$limit,$count,$id)
    {      
       
         $ci  =&get_instance();
         $ci->load->library("pagination");      
         $ci->load->model($modelname); 
               
         $limits_custom=($ci->input->post('droppag')) ? $ci->input->post('droppag') : 10; 
        $config = array();
        $config["per_page"] = $limits_custom;       
        $config["base_url"] = base_url() . $base_url;
        $config["total_rows"] = $count;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['num_tag_open'] = "<li>";
        $config['num_tag_close'] = "</li>";
        $config['cur_tag_open'] = "<li class='active'><a>";
        $config['cur_tag_close'] = "</a></li>";

        $ci->pagination->initialize($config);
        $seg = '';
        if($id != null)
        {
            $seg=4;
        }
        else
        {
           $seg=3;
        }




        $page = ($ci->uri->segment($seg)) ? $ci->uri->segment($seg) : 0;
       
        if($id != null)
        {
            $data1 = $ci->$modelname->$selectfunname($id,$config["per_page"],$page); 
        }
        else
        {
            $data1 = $ci->$modelname->$selectfunname(null,$config["per_page"],$page);
        }
      
        $links=$ci->pagination->create_links();
        $per_page = $config["per_page"];
        $current_page = $ci->uri->segment($seg);
        $total_entries = $config["total_rows"];

        $end = $current_page + $per_page;
        $end = ($end > $total_entries) ? $total_entries : $end;
  
        $stirng_pagination="Showing ".($current_page+1)." to ".$end." of ".$total_entries." entries";
        $ci->smartyci->assign('pagination_array', $links);
        $ci->smartyci->assign('segment_uri', $ci->uri->segment($seg,0));
        $ci->smartyci->assign('record_string',  $stirng_pagination); 
        return $data1;
   } 
    
}