<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


		// Get main CodeIgniter object
		$ci =& get_instance();	   
		

		// Load All Libraries Here
		$ci->load->library('Json');
		$ci->load->library('form_validation');
		
		
		// Load All Helpers Here
		$ci->load->helper('dropdown');
		$ci->load->helper('breadcrumb');
		$ci->load->helper('audit_log');
		$ci->load->helper('date');
		
	   
	   // Set All CSS plugins
        $css_plugin = array(

        			
					
					'bootstrap/dist/css/bootstrap.min.css',
					'fontawesome/css/font-awesome.min.css',
					'google/google_font_open_sans.css?family=Open+Sans:400,300,600,700',
					'google/google_font_oswald.css?family=Oswald:700,400',
					'animate.css/animate.min.css',
					'jquery.gritter/css/jquery.gritter.css',
					
					'datatables/css/dataTables.bootstrap.css',
					'datatables/css/datatables.responsive.css',
					'datatables/css/fixedColumns.dataTables.min.css',
					'datatables/css/fixedHeader.dataTables.min.css',

					'clockpicker/dist/bootstrap-clockpicker.css',
					'clockpicker/dist/bootstrap-clockpicker.min.css',
					'clockpicker/dist/jquery-clockpicker.css',
					'clockpicker/dist/jquery-clockpicker.min.css',
					
					'admin/css/pages/invoice.css',
					'admin/css/reset.css',
					'admin/css/layout.css',
					'admin/css/components.css',
					'admin/css/plugins.css',
					'admin/css/themes/black-and-white.theme.css',
					'bootstrap-calendar/css/calendar.css',
					'summernote/dist/summernote.css',
					'bootstrap-wysihtml5/src/bootstrap-wysihtml5.css',
					

					

					
			

        );
		
        $ci->smartyci->assign('list_css_plugin',$css_plugin);


		
		// Set All CSS page
        $css_page = array(
            'sign.css'
        );
        $ci->smartyci->assign('list_css_page',$css_page);
		
		
		
        // Set All JS plugins
        $js_plugin = array(


        		'custom/js/notify.min.js',
				'custom/js/jquery.form.min.js',		
				'custom/js/custom.js',
				
				'html5shiv/dist/html5shiv.min.js',
				'respond-minmax/dest/respond.min.js',
				'multifilter/multifilter.js',
				'multifilter/multifilter_function.js',
				'bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js',
				'jsTimezoneDetect/jstz.min.js',
				'jquery.gritter/js/jquery.gritter.min.js',
				'jquery-cookie/jquery.cookie.js',				

				'typehead.js/dist/handlebars.js',
				'typehead.js/dist/typeahead.bundle.min.js',
				'jquery-nicescroll/jquery.nicescroll.min.js',
				'jquery.sparkline.min/index.js',
				'jquery-easing-original/jquery.easing.1.3.min.js',
				'ionsound/js/ion.sound.min.js',
				'bootbox/bootbox.js',
				'datatables/js/jquery.dataTables.min.js',
				'datatables/js/dataTables.bootstrap.js',
				'datatables/js/datatables.responsive.js',
				'datatables/js/dataTables.fixedHeader.min.js',
				'datatables/js/dataTables.fixedColumns.min.js',				
				'fuelux/dist/js/fuelux.min.js',
				'jquery-nicescroll/jquery.nicescroll.min.js',
				'datepicker/bootstrap-datepicker.min.js',
				'jquery-validation/dist/jquery.validate.min.js',				
				'datatables/js/jQuery.dtplugin.js',
				'bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.min.js',
				'bootstrap-wysihtml5/src/bootstrap-wysihtml5.js',
				'summernote/dist/summernote.min.js'	
				

				
		
        );

        if($ci->uri->segment('1') != 'Dashboard')
        {
        	 $js_plugin[]='bootstrap/dist/js/bootstrap.min.js';
        }
		
        $ci->smartyci->assign('list_js_plugin',$js_plugin);

        $js_plugin_admin = array(
        	// for all admin related js files here 
		);
		$ci->smartyci->assign('list_js_plugin_admin',$js_plugin_admin);
		
		
		
        // Set JS All page
        $js_page = array(
			
        );
        $ci->smartyci->assign('list_js_page',$js_page);

	
?>