<?php
if(!function_exists('sendmail'))
{

  function sendmail($to,$cc,$bcc,$subject,$mail_content,$attachment)
  {       
        // config of mail of CI
        $ci  =&get_instance();
        $ci->load->library('email');
        $config['mailtype'] = 'html';
        $config['priority'] = '1';
        $config['bcc_batch_mode'] = 'TRUE';
        $config['validate'] = 'TRUE';
        $ci->email->initialize($config);

        // from mail sending
        $ci->email->from('contactsmuc54@gmail.com', 'Trusto Healthcare');

        // to address send mail
        $ci->email->to($to);

        if($cc != '')
        $ci->email->cc($cc);
        if($bcc != '')
        $ci->email->bcc($bcc);

        // subject for send mail

        $ci->email->subject($subject);     


        // attachment file to send mail
        if(count($attachment) > 1)
        {
            foreach($attachment as $filepath)
            {
              $ci->email->attach($filepath);
            }
        }
        elseif(count($attachment) == 1)
        {
           $ci->email->attach($attachment[0]);
        }
        // Mail Content to send mail          

        $ci->email->message($mail_content);
 


        
        if($ci->email->send())
        {
          
           $ci->email->clear(TRUE);
           //return true;
        }
        else
        {         
           //return false;
        }

      //print_r($this->email->print_debugger());
  }

}
?>