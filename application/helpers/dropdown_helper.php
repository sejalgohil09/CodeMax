<?php
if(!function_exists('dropdown')){

function dropdown($tablename,$where,$order)
{
  $sort='ASC';
  $ci = &get_instance();
  $ci->load->database();
  $tablename=$tablename;
  $where=$where;
  if($where == null)
  {
     $ci->db->from($tablename);
     $ci->db->order_by($order,$sort);
     $query = $ci->db->get();
     
  } 
  if($where != null)
  {
    $query=$ci->db->order_by($order,$sort)->get_where($tablename,$where);  
     
  }   
  return $query->result();
}
  
}
 
?>
