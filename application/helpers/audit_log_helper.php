<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('audit_log')){
   function audit_log($module,$task,$data)
   {
       //get main CodeIgniter object
       $ci =& get_instance();
       
       //load databse library
       $ci->load->database();

       //date helper
       $ci->load->helper('date');
       date_default_timezone_set('Asia/Kolkata');
   	   $ip=$ci->input->ip_address();
   	   
       $user_id=$ci->session->userdata('user_id');
   	   $module=$module;
   	   $task=$task;	   
   	   $data=implode(",",$data);
	   switch ($task) {
		    case "Add":
		        $note="New Entry row with ID :".$data.", Has Been Added Successfully";
		        break;
		    case "Edit":
		         $note="Edit row with ID :".$data.", Has Been Changed Successfully";
		        break;
		    case "Delete":
		         $note="Delete row with ID :".$data.", Has Been Deleted Successfully";
		        break;
		    case "Change Password":
				 $note="Change Password row with ID :".$data.",Has Been Changed Successfully";
				break;		
			case "Login":
				 $note="Login row with ID :".$data.",Has Been Logged In Successfully";
				break;
			case "Logout":
				 $note="Logout row with ID :".$data.",Has Been Logged Out Successfully";
				break;			
			default:
		         $note= "Audit log entry failed";
		}
       $date = date('Y-m-d H:i:s'); 

		$data_vlaue= array(
		'ipaddress' => $ip ,
		'user_id' => $user_id ,
		'module' => $module,
		'task' => $task,
		'note' => $note,
		'logdate' => $date
		);
		$ci->db->insert('_logs', $data_vlaue); 
		$ci->session->set_flashdata('item', $note);
   }	
}
?>