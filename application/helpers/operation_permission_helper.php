<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('operation_permission')){
   function operation_permission()
   {
   		$CI = & get_instance();  //get instance, access the CI superobject
  		$user_level = $CI->session->userdata('user_level');  	
  		$check_opr = "SELECT operation_permission FROM _userlevels WHERE id='".$user_level."'";
        $opr_res = $CI->db->query($check_opr);
        $opr_str=$opr_res->row();
        $array_OPR=explode(',',$opr_str->operation_permission);
        $CI->smartyci->assign('OPERATION',$array_OPR);
   }
}