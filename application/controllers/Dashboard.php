<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller 
{
      function __construct()
     {
            parent::__construct();
			$this->load->helper('init');
            $this->load->model('Dashboard_model','dm');
            $this->load->model('Models_model','msm');
     }

     public function index()
     {
        // Set title page
        $this->smartyci->assign('title', 'DASHBOARD'); 

        // Set content page
        $this->smartyci->assign('body', 'contents/dashboard.html');       
        
        // Set active menu        
        $this->smartyci->assign('active_dashboard', 'active');

        // Render view on main layout
        $this->smartyci->display('contents/layout.html');
    }

   public function ajax_list()
   {
        $result .=' <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Mfg Name</th>
                                            <th>Model Name</th>                                           
                                            <th>Count</th>                        
                                            <th class="text-center" style="min-width: 15%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tablebody">';
         $lists = $this->db->select('ms.*,m.mfg_name')->join('manufacturers m','m.id=ms.mfg_id')->from('models ms')->where('qty > ','0')->get()->result();
         $i=1;
         if(count($lists) > 0)
         {  
            foreach($lists as $val)
            {
            $result .='<tr>
                <td class="text-center text-danger">'.$i.'</td>
                <td class="text-danger">'.$val->mfg_name.'</td>
                <td class="text-danger">'.$val->name.'</td>
                <td class="text-danger">'.$val->qty.'</td>    
                <td class="text-center text-danger">                                                
                    <button onclick="sold('.$val->id.')">sold</button>
                </td>
            </tr>';
            $i=$i+1;
            }


         }
         else
         {
            $result.='<td colspan="5">No Record Found</td>';
         }  
         $result .='</tbody></table>';      
           
        echo $result;                                
         
   }

   public function sold($id)
   {
      $res=$this->db->select('qty')->get_where('models',array('id'=>$id))->row();
      

      if($res->qty != 0)
      {
         $qty=$res->qty-1;
         $result = $this->db->update('models',array('qty'=> $qty),"id=".$id."");
      }
       
   // echo  $this->db->last_query();exit;

   }


    


}
?>