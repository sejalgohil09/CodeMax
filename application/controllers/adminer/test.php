<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class test extends CI_Controller {

    public function index()
    {
      
        // Set title page
        $this->smartyci->assign('title', 'DATATABLE');

        // Set CSS plugins
        $css_plugin = array(
            'datatables/css/dataTables.bootstrap.css',
            'datatables/css/datatables.responsive.css',
            'fuelux/dist/css/fuelux.min.css'
        );
        $this->smartyci->assign('list_css_plugin',$css_plugin);

        // Set JS plugins
        $js_plugin = array(
            'datatables/js/jquery.dataTables.min.js',
            'datatables/js/dataTables.bootstrap.js',
            'datatables/js/datatables.responsive.js',
            'fuelux/dist/js/fuelux.min.js'
        );
        $this->smartyci->assign('list_js_plugin',$js_plugin);

        // Set JS page
        $js_page = array(
            'blankon.table.js'
        );

        $this->smartyci->assign('list_js_page',$js_page);

        $this->load->model('userlevels_model');
        $this->data['recordset']=$this->userlevels_model->select($d=null);
        $data_records= $this->data['recordset'];

         $this->load->helper('breadcrumb');

        // Set content page
        $this->smartyci->assign('body', 'contents/test/view.html'); 
        $this->smartyci->assign('array1', $data_records);
        
        // Set active menu
        $this->smartyci->assign('active_forms', 'active');
        $this->smartyci->assign('active_form_validation', 'active');

        // Render view on main layout
        $this->smartyci->display('contents/layout.html');
      
    }
}