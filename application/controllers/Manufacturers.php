<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manufacturers extends CI_Controller {
      public function __construct()
       {
            parent::__construct(); 
      			$this->load->helper('init');
            //load model 
            $this->load->model('Manufacturers_model','mm'); 
       }

    public function index()
    {     
      $this->smartyci->assign('title', 'Manufacturers Master');

      // Set content page
      $this->smartyci->assign('body', 'contents/manufacturers/list.html');
        
		  // Set active menu
      $this->smartyci->assign('active_mfg', 'active');
        
      // Render view on main layout
      $this->smartyci->display('contents/layout.html');
    }

    public function ajax_list()
    {
        $lists = $this->mm->get_datatables();
       

        $output = array();
        $no = $_POST['start'];

        foreach ($lists as $list)
        {
            $no++;

            //add html for action
           
            $edit_button = '<a href="'.base_url().'Manufacturers/form/'.$list->id.'" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>';
            $row = array();
            $row[] = '<input type="checkbox" class="data-check" value="'.$list->id.'"> '. "&nbsp;&nbsp;".$edit_button;
            $row[] = $no;
            $row[] = $list->mfg_name;
            $row[] = $list->is_active;            
           
            $output[] = $row;
            
        }

        $recordsTotal = $this->mm->count_all();

        $recordsFiltered = $this->mm->count_filtered();

        $this->json->add("recordsTotal", $recordsTotal);
        $this->json->add("recordsFiltered",$recordsFiltered);
        $this->json->add("data",$output);

        $this->json->echoJSON();      
        
        
    }


    public function form()
    {
        // Set title page
        $this->smartyci->assign('title', 'Manufacturers Form');

        $this->smartyci->assign('body', 'contents/manufacturers/form.html');

        if($this->uri->segment(3) != "")
        { 
          $id=$this->uri->segment(3);
          $data=$this->mm->select($id);
          $this->smartyci->assign('mfg_master', $data); 
        }
        else
        {
          $id="new";
        }		 

        $this->smartyci->assign('id', $id);
        $this->smartyci->assign('Manufacturers', 'active');
        $this->smartyci->assign('Manufacturers', 'active');


        // Render view on main layout
        $this->smartyci->display('contents/layout.html');

     }

      public function add_record()
       { 
			     $all=$this->mm->form();
       }

    public function ajax_bulk_delete()
    {
        $list_id = explode(',',$this->input->post('list_id'));
        foreach ($list_id as $id) {
            $this->mm->delete_by_id($id, 'manufacturers');
        }
        echo json_encode(array("status" => TRUE));
    }
		      
}

