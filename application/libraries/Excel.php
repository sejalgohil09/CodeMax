<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Excel{

    private $excel;

    public function __construct()
    {
    require_once APPPATH . 'third_party/PHPExcel.php';
    $this->excel = new PHPExcel();

    $this->excel->getProperties()->setCreator("Clarus Info Solution")
                    ->setLastModifiedBy(date("Y-m-d"))
                     ->setSubject("Export")
                    ->setDescription("Export Data From Database")
                    ->setKeywords("Export")
                    ->setCategory("Sales Multiplier");

    }
    public function load($path) {
       $objReader = PHPExcel_IOFactory::createReader('Excel5');
       $this->excel =$objReader->load($path);
    }

    public function save($path) 
    {
       $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
       $objWriter->save($path);
    }

    public function stream1($filename,$data = null,$sheet_name)
      {   
          
        $count_data_size=count($sheet_name)-1;
        
          for($i=0;$i<=$count_data_size;$i++)
          {              
               
                $this->excel->createSheet();
                $this->excel->setActiveSheetIndex($i);                          
                $this->excel->getActiveSheet($i)->setTitle($sheet_name[$i]);

                if ($data[$i] != null)
                {                  
                    $col = 'A';
                    foreach ($data[$i][0] as $key=>$cell) 
                    {
                      
                      $objRichText = new PHPExcel_RichText();
                      $objPayable =$objRichText->createTextRun(ucwords(str_replace("_", " ",$key)));
                       $this->excel->getActiveSheet()->getStyle($col . '1')->getAlignment()->setIndent(1);

                      $this->excel->getActiveSheet()->getStyle($col . '1')->getFont()->setBold(true);
                        $this->excel->getActiveSheet()
                                    ->getStyle($col . '1')
                                    ->applyFromArray(
                                                      array(
                                                              'fill' => array(
                                                              'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                                              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                                              'bold'  => true,
                                                              'color' => array('rgb' => 'dd4814'),
                                                              'size'  => 15,
                                                              'name'  => 'Verdana'
                                                                  )
                                                             )
                                                       );
                       PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
                      $this->excel->getActiveSheet($i)->getCell($col . '1')->setValue($objRichText);
                      $col++;
                      
                    }
                 
                    $rowNumber = 2;
                    foreach ($data[$i] as $row)
                    {                     
                      $col = 'A';
                      foreach ($row as $cell)
                      {
                      $this->excel->getActiveSheet($i)->setCellValue($col .$rowNumber,$cell);
                      $col++;
                      }
                    $rowNumber++;
                    }
                }

          }     
          //exit;
            

      }
    public function stream($filename,$data = null)
    {
        $date=date('d-m-Y');
        $this->excel->getProperties()->setTitle($filename);
        $this->excel->setActiveSheetIndex();                  
        $this->excel->getActiveSheet()->setTitle(ucwords(str_replace("_".$date, " ",str_replace(".xls"," ",$filename))));
        if ($data != null) {
          $col = 'A';
          foreach ($data[0] as $key =>$val) 
          {
            $objRichText = new PHPExcel_RichText();
            $objPayable =$objRichText->createTextRun(ucwords(str_replace("_", " ",$key)));
  $this->excel->getActiveSheet()->getStyle($col . '1')->getAlignment()->setIndent(1);
                      $this->excel->getActiveSheet()->getStyle($col . '1')->getFont()->setBold(true);
                        $this->excel->getActiveSheet()
                                    ->getStyle($col . '1')
                                    ->applyFromArray(
                                                      array(
                                                              'fill' => array(
                                                              'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                                              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                                              'bold'  => true,
                                                              'color' => array('rgb' => 'dd4814'),
                                                              'size'  => 15,
                                                              'name'  => 'Verdana'
                                                                  )
                                                             )
                                                       );
            $this->excel->getActiveSheet()->getCell($col . '1')->setValue($objRichText);
            $col++;
          }
          $rowNumber = 2;
            foreach ($data as$row)
            {
              $col = 'A';
              foreach ($row as $cell) {
              $this->excel->getActiveSheet()->setCellValue($col .$rowNumber,$cell);
              $col++;
            }
          $rowNumber++;
          }

          $download=$this->download_excel($filename);
        }  

        return $download;


    }

    public function download_excel($filename)
    {

        header('Content-type: application/ms-excel');
        header('Content-Description: File Transfer');
        header("Content-Disposition: attachment; filename=\"".$filename."\"");
        header("Cache-control: private");
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: no-cache');
            
        ob_end_clean();
        flush();
         $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
         $objWriter->save('php://output');

        header("location: " . base_url() . "export/$filename");
        unlink(base_url() . "export/$filename");
        
    }

        public function __call($name,$arguments) {
        if (method_exists($this->excel,$name)) {
            return call_user_func_array(array($this->excel,$name),$arguments);
        }
        return null;
    }

    public function save_excelfile($filename)
    {
       
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');        
        $objWriter->save(str_replace(__FILE__,'export/'.$filename,__FILE__));

    }



    public  function  run_import() 
    {
       $file    = explode ( '.' ,$_FILES ['database']['name']);
       $length  = count ($file );
        if ($file [$length-1] =='.xlsx'  ||$file [$length-1] == 'xls' ) 
        { 
          $tmp =$_FILES ['database'] ['tmp_name'];
          $this->load->library( 'excel' ); 
          $read  = PHPExcel_IOFactory::createReaderForFile ($tmp );
          $read -> setReadDataOnly(true);
          $excel   =$read->load($tmp );
          $sheets  =$read->listWorksheetNames($tmp ); // read all existing sheet
            foreach ($sheets as $sheet )
             {
                if ($this->db->table_exists($sheet))
                 { 
                  $_sheet  = $excel -> setActiveSheetIndexByName ($sheet ); // Key sheetnye kagak let loose :-p
                  $maxRow  = $_sheet -> getHighestRow ();
                  $maxCol  = $_sheet -> getHighestColumn ();
                  $fields  = array ();
                  $sql     = array ();
                  $maxCol  = range ( 'A' ,$maxCol );
                    foreach ($maxCol  as$key  =>$coloumn ) 
                    {
                      $fields[$key] = $_sheet->getCell($coloumn . '1')->getCalculatedValue (); // first column as the field list on table
                    }
                    for ($i=2;$i<=$maxRow;$i++)
                     {
                        foreach ($maxCol as $k=>$coloumn)
                         {
                           $sql[$field[$k]]=$_sheet->getCell($coloumn.$i)->getCalculatedValue ();
                        }
                       $this -> db-> insert ($sheet,$sql );  
                    }
                }
            }
        } 
        else
         {
            exit ( 'do not allowed to upload' ); // error message is not appropriate file type
         }
        redirect ( 'home' ); // redirect the after success
    }

}