<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Json{
	
	private $jsonData,$jsonArr;
	function __construct()
	{
		$this->jsonArr = array();
		}
	function prepareJSON()
	{
		$this->jsonData = json_encode($this->jsonArr);
	}
	function echoJSON()
	{
		$this->prepareJSON();
		echo $this->jsonData;
		exit;
	}
	function getJSON()
	{
		$this->prepareJSON();
		return $this->jsonData;
	}
    function getArray()
    {
        return $this->jsonArr;
    }
	function add($key,$val)
	{
		$this->jsonArr[$key] = $val;
	}
    function sendJSON($message,$success=false)
    {
        $this->add("message",$message);
        $this->add("success",$success);
        $this->echoJSON();
    }
	function send($message,$success=false)
	{
		$this->sendJSON($message,$success);
	}
}