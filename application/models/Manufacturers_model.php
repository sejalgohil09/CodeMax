<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manufacturers_model extends CI_Model {

	var $table = 'manufacturers m';
	var $column_order = array(null,'id','mfg_name','is_active'); //set column field database for datatable orderable
	var $column_search = array('id','mfg_name','is_active'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	
	var $count_all = 0;
	var $count_filtered = 0;

    function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
	{
		
		$query = $this->db->from('manufacturers');
		

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		
	}
	function filter()
	{

		
			$columns = array( 
						// datatable column index  => database column name
							0 => '',
							1 => 'id',
							2 => 'mfg_name',
							3 => 'is_active',
						);
	

		$requestData= $_REQUEST;
		$i = 0;		
		foreach ($columns as $item) // loop column 
		{	
			if( !empty($requestData['columns'][$i]['search']['value']) ){   
				$this->db->like($item, $requestData['columns'][$i]['search']['value']);
			}
				
			$i++;
		}
	}

	function comman_search()
	{
		$requestData= $_REQUEST;
		$serach_val=trim($requestData['search']['value']);
		
		for($i=2;$i<count($this->column_search);$i++) // loop column 
		{
			if(!empty($requestData['search']['value']))
			{
			    $this->db->or_like($this->column_search[$i],$serach_val);					
			}
			
		}
		
	} 
	function get_datatables()
	{
		$this->_get_datatables_query();
		$this->filter();
		$this->comman_search();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		$res= $query->result();	

		return $res;
		//echo $this->db->last_query();	exit;
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$this->filter();
		$this->comman_search();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

    function select($id)
    {
		
       if($id != null)
        {
         	  
            $query = "SELECT * FROM manufacturers WHERE id = '".$id."'";
            $result = $this->db->query($query);
            return $result->result();
        }   
        else
        {
            $query = $this->db->get('manufacturers');
            $result =$this->db->query($query);
            return $result->result();  
        }
    }

    function form()
    {
			$id=$this->input->post('id');
			$mfg_name=$this->input->post('mfg_name');
			$is_active=$this->input->post('is_active');
			


			$data = array(
					'mfg_name' => $mfg_name,					
					'is_active' => $is_active,					
   				);
			
			if($id == "new")
            {
				
				$this->db->insert('manufacturers', $data);
				$id=$this->db->insert_id();
				
				$task = "Add";
            }
            else
            {
                $this->db->update('manufacturers', $data, "id = $id");
				
				$task = "Edit";
            }

            $data=array($id);
            $module='Manufacturers';
            audit_log($module,$task,$data);

           
           
			$url = base_url()."Manufacturers/";
			$this->json->add("redirect",$url);
			$this->json->send("Manufacture Added successfully",true);			

    }
     
    public function delete_by_id($id, $table)
	{
					    
		$this->db->where('id', $id);
		$this->db->delete($table);
		
	}
}
