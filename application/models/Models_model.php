<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Models_model extends CI_Model {

	var $table = 'models m';
	var $column_order = array(null,'mfg_id','model_no','name','description','qty','price','is_active'); //set column field database for datatable orderable
	var $column_search = array('id','mfg_name','model_no','name','description','qty','price','is_active'); //set column field database for datatable searchable just firstname , lastname , address are searchable
	
	var $count_all = 0;
	var $count_filtered = 0;

    function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
	{
		
		$query = $this->db->select('ms.*,m.mfg_name')->join('manufacturers m','m.id=ms.mfg_id')->from('models ms');
		

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		
	}
	function filter()
	{

		
			$columns = array( 
						// datatable column index  => database column name
							0 => '',
							1 => 'id',
							2 => 'mfg_name',
							3 => 'model_no',
							4 => 'name',
							5 => 'description',
							6 => 'qty',
							7 => 'price',
							8 => 'is_active'
						);
	

		$requestData= $_REQUEST;
		$i = 0;		
		foreach ($columns as $item) // loop column 
		{	
			if( !empty($requestData['columns'][$i]['search']['value']) ){   
				$this->db->like($item, $requestData['columns'][$i]['search']['value']);
			}
				
			$i++;
		}
	}

	function comman_search()
	{
		$requestData= $_REQUEST;
		$serach_val=trim($requestData['search']['value']);
		
		for($i=2;$i<count($this->column_search);$i++) // loop column 
		{
			if(!empty($requestData['search']['value']))
			{
			    $this->db->or_like($this->column_search[$i],$serach_val);					
			}
			
		}
		
	} 
	function get_datatables()
	{
		$this->_get_datatables_query();
		$this->filter();
		$this->comman_search();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();		
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$this->filter();
		$this->comman_search();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

    function select($id)
    {
		
       if($id != null)
        {
         	  
            $query =  $this->db->select('ms.*,m.mfg_name,m.id')->join('manufacturers m','m.id=ms.mfg_id')->from('models ms');
            $query = $this->db->get();
		    return $query->result();
        }   
        else
        {
            $query =  $this->db->select('ms.*,m.mfg_name,m.id')->join('manufacturers m','m.id=ms.mfg_id')->from('models ms');
            $query = $this->db->get();
		    return $query->result();
        }
    }

    function form()
    {
			$id=$this->input->post('id');
			$mfg_id=$this->input->post('mfg_id');
			$model_no=$this->input->post('model_no');
			$name=$this->input->post('name');
			$qty=$this->input->post('qty');
			$price=$this->input->post('price');
			$is_active=$this->input->post('is_active');
			
			$data = array(
					'mfg_id' => $mfg_id,					
					'model_no' => $model_no,					
					'name' => $name,					
					'qty' => $qty,					
					'price' => $price,					
					'is_active' => $is_active,					
   				);
			
			if($id == "new")
            {
				
				$this->db->insert('models', $data);
				$id=$this->db->insert_id();
				
				$task = "Add";
            }
            else
            {
                $this->db->update('models', $data, "id = $id");
				
				$task = "Edit";
            }

            $data=array($id);
            $module='models';
            audit_log($module,$task,$data);

           
           
			$url = base_url()."models/";
			$this->json->add("redirect",$url);
			$this->json->send("Models Added successfully",true);			

    }
     
    public function delete_by_id($id, $table)
	{
					    
		$this->db->where('id', $id);
		$this->db->delete($table);
		
	}
}
