<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-04-28 12:23:58
         compiled from "D:\xampp\htdocs\car\application\views\templates\contents\rate_list\list.html" */ ?>
<?php /*%%SmartyHeaderCode:112635ae41a867041f7-29022202%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3b41b3177e8cd11a6e8d41b1f662200a3f57ec30' => 
    array (
      0 => 'D:\\xampp\\htdocs\\car\\application\\views\\templates\\contents\\rate_list\\list.html',
      1 => 1512543322,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '112635ae41a867041f7-29022202',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'counter' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ae41a8688ac54_24936982',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ae41a8688ac54_24936982')) {function content_5ae41a8688ac54_24936982($_smarty_tpl) {?>
<?php echo generateBreadcrumb();?>

<div class="body-content animated fadeIn">
        
            <!-- Start datatable using dom -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                    <?php if ($_SESSION['user_level']<3) {?>
                        <a href="<?php echo base_url('Rate_list/form');?>
">
                          <button type="button" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span> Add New Rate List</button>
                          </a>
                    <?php }?>
                        <?php if ($_SESSION['user_level']==1) {?>
                        <button class="btn btn-danger " onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i>  Bulk Delete</button>
                        <?php }?>
                        <?php if ($_SESSION['user_level']==1) {?>
                         <a href="<?php echo base_url('Rate_list/export');?>
"><button  type="button" class="btn btn-primary">Export</button></a>
                         <a href="<?php echo base_url('Rate_list/import');?>
"><button  type="button" class="btn btn-primary">Import</button></a>
                         <?php }?>
                         
                       
                    </div>                
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="clearfix"></div>
                <div class="panel-body double-scroll">
                    <table id="table" class="table table-striped table-bordered table-theme" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:80px;"><input type="checkbox" id="check-all"> &nbsp; Action</th>
                                <th class="text-center">Sr No.</th> 
                                <th class="text-center">Lab Code</th>
                                <th class="text-center">Test Code</th>
                                <th class="text-center">Test Name</th> 
                                <th class="text-center">Basic Rate</th>
                                <th class="text-center">Discount Rate</th>
                                <th class="text-center">Loyalty Discount Rate</th>                       
                                <th class="text-center">Lab</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">Sub Type</th>
                                <th class="text-center">Frequent</th>
                                <th class="text-center">All Inclusive Package</th>
                                <th class="text-center">Not Available Booking</th>
                                <th class="text-center">Fasting</th> 
                                <th class="text-center">Frequency</th>
                                <th class="text-center">Lab Rate</th>
                                <th class="text-center">Specimen Type</th>
                                <?php if ($_SESSION['user_level']<2) {?>
                                <th class="text-center">B2B Rate</th>
                                <?php }?>
                                <th class="text-center">Descrption</th>
                                <th class="text-center">D_ND</th>
                                <th class="text-center">F_NF</th>
                                <th class="text-center">Cpl Tests</th>
                                <th class="text-center">Cpl Code</th>
                            </tr>
                        </thead>
                        <thead>
                           <tr>
                           <?php $_smarty_tpl->tpl_vars["counter"] = new Smarty_variable(2, null, 0);?>
                           
                            <th> &nbsp; </th>
                            <th> &nbsp; </th>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>


                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;">
                                <select class="search-input-select" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
">
                                    <option selected="true" value="">Please Select </option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;">
                                <select class="search-input-select" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
">
                                    <option selected="true" value="">Please Select </option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;">
                                <select class="search-input-select" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
">
                                    <option selected="true" value="">Please Select </option>
                                    <option value="1">Fasting</option>
                                    <option value="2">Non Fasting</option>
                                </select></th>

                            

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>


                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php if ($_SESSION['user_level']<=1) {?>
                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>
                            <?php }?>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;">
                                <select class="search-input-select" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
">
                                    <option selected="true" value="">Please Select </option>
                                    <option value="1">D</option>
                                    <option value="2">ND</option>
                                </select></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;">
                                <select class="search-input-select" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
">
                                    <option selected="true" value="">Please Select </option>
                                    <option value="1">F</option>
                                    <option value="2">NF</option>
                                </select></th>

                             <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>
                             <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Action</th>
                                <th class="text-center">Sr No.</th> 
                                <th class="text-center">Lab Code</th>
                                <th class="text-center">Test Code</th>
                                <th class="text-center">Test Name</th> 
                                <th class="text-center">Basic Rate</th>
                                <th class="text-center">Discount Rate</th> 
                                <th class="text-center">Loyalty Discount Rate</th>                     
                                <th class="text-center">Lab</th>
                                <th class="text-center">Type</th>
                                <th class="text-center">Sub Type</th>
                                <th class="text-center">Frequent</th>
                                <th class="text-center">All Inclusive Package</th>
                                <th class="text-center">Not Available Booking</th>
                                <th class="text-center">Fasting</th> 
                                <th class="text-center">Frequency</th>
                                <th class="text-center">Lab Rate</th>
                                <th class="text-center">Specimen Type</th>
                                <?php if ($_SESSION['user_level']<=1) {?>
                                <th class="text-center">B2B Rate</th>
                                <?php }?>
                                <th class="text-center">Descrption</th>
                                <th class="text-center">D_ND</th>
                                <th class="text-center">F_NF</th> 
                                <th class="text-center">Cpl Tests</th>                     
                                <th class="text-center">Cpl Code</th>                     
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.panel-body -->
            </div>

    </div><!-- /.row -->

<?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('assets/custom_js/jquery-ui.min.js');?>
"><?php echo '</script'; ?>
>  
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('assets/custom_js/jquery.doubleScroll.js');?>
"><?php echo '</script'; ?>
> 
        
          <?php echo '<script'; ?>
 type="text/javascript">
            $(document).ready(function(){
               $('.double-scroll').doubleScroll();
            });
   <?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" >
        $(document).ready(function() {
             var userlevel='<?php echo $_SESSION['user_level'];?>
';
    if(userlevel <= '1')
    {

      var sorted=[0];
    }
    else
    {
      sorted=[18];
    }

            var dataTable = $('#table').DataTable( {
                "processing": true,
                "serverSide": true,
                "columnDefs": [{ "orderable": false, "targets": sorted }],
                "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
                "ajax":{
                    url :"<?php echo base_url();?>
Rate_list/ajax_list", // json datasource
                    type: "post",  // method  , by default get
                    error: function(){  // error handling
                        $(".table_error").html("");
                        $("#table").append('<tbody class="table_error"><tr><th colspan="3"> No Controller or Method Name Found</th></tr></tbody>');                     
                    },

                }

            } );
                //comman search             
            $('.form-control').keyup('keyup',function(){
                dataTable.search($(this).val()).draw();
            });
            $('.search-input-text').on( 'keyup', function () {   // for text boxes
                var i =$(this).attr('data-column');  // getting column index
                var v =$(this).val();  // getting search input value
                dataTable.columns(i).search(v).draw();
            });
            $('.search-input-select').on( 'change', function () {   // for select box
                
                var i =$(this).attr('data-column');  
                var v =$(this).val();  
                dataTable.columns(i).search(v).draw();
            } );
            
            
            
        } );



    function bulk_delete()
    {
        var list_id = [];
        $(".data-check:checked").each(function() {
                list_id.push(this.value);
        });
        
        if(list_id.length > 0)
        {
            if(confirm('Are you sure delete this '+list_id.length+' data?'))
            {
                // console.log(list_id);
                $.ajax({
                    type: "POST",
                    data: "list_id="+list_id,
                    url: "<?php echo base_url();?>
Rate_list/ajax_bulk_delete",
                    dataType: "JSON",
                    success: function(data)
                    {
                        if(data.status)
                        {
                            console.log(data);
                            reload_table();
                        }
                        else
                        {
                            console.log(data);
                            alert('Failed.');
                        }
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log(data);
                        alert('Error deleting data');
                    }
                });
            }
        }
        else
        {
            alert('no data selected');
        }
    }

function reload_table()
{
    alert('Delete Successfully');
    $('#table').DataTable().ajax.reload(null, false).draw(); //reload datatable ajax 
}


     //check all
    $("#check-all").click(function () {
        $(".data-check").prop('checked', $(this).prop('checked'));
    });


        <?php echo '</script'; ?>
><?php }} ?>
