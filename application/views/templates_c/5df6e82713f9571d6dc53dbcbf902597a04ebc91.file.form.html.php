<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-04-28 16:45:48
         compiled from "D:\xampp\htdocs\car\application\views\templates\contents\models\form.html" */ ?>
<?php /*%%SmartyHeaderCode:42415ae445ef438bf3-11491049%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5df6e82713f9571d6dc53dbcbf902597a04ebc91' => 
    array (
      0 => 'D:\\xampp\\htdocs\\car\\application\\views\\templates\\contents\\models\\form.html',
      1 => 1524914146,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '42415ae445ef438bf3-11491049',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ae445ef54e1b4_30803295',
  'variables' => 
  array (
    'id' => 0,
    'model_master' => 0,
    'val' => 0,
    'mfg_list' => 0,
    'mfg_id' => 0,
    'model_no' => 0,
    'name' => 0,
    'qty' => 0,
    'price' => 0,
    'is_active' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ae445ef54e1b4_30803295')) {function content_5ae445ef54e1b4_30803295($_smarty_tpl) {?><!-- Start page header -->

    <?php if ($_smarty_tpl->tpl_vars['id']->value!="new") {?>
    <?php $_smarty_tpl->tpl_vars["mfg_id"] = new Smarty_variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["model_no"] = new Smarty_variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["name"] = new Smarty_variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["description"] = new Smarty_variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["qty"] = new Smarty_variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["price"] = new Smarty_variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["is_active"] = new Smarty_variable('', null, 0);?>
    

    <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['model_master']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
?>
      <?php $_smarty_tpl->tpl_vars['mfg_id'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->mfg_id, null, 0);?>
      <?php $_smarty_tpl->tpl_vars['model_no'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->model_no, null, 0);?>
      <?php $_smarty_tpl->tpl_vars['name'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->name, null, 0);?>
      <?php $_smarty_tpl->tpl_vars['description'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->description, null, 0);?>     
      <?php $_smarty_tpl->tpl_vars['qty'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->qty, null, 0);?>     
      <?php $_smarty_tpl->tpl_vars['price'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->price, null, 0);?>     
      <?php $_smarty_tpl->tpl_vars['is_active'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->is_active, null, 0);?>     
    <?php } ?>
    <?php }?>
<div class="body-content animated fadeIn">
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel rounded shadow">
				<div class="panel-heading">
					<div class="pull-left">
						<h3 class="panel-title">Add New Model</h3>
					</div><!-- /.pull-left -->              
					<div class="clearfix"></div>
				</div><!-- /.panel-heading -->
				<div class="panel-body no-padding">
					<form class="form-horizontal" id="user_submit_form" role="form" method="POST"  name="user_submit_form" action="<?php echo base_url('Models/add_record');?>
"">
					
					<input type="hidden" class="form-control" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">

					<div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" >Mfg Name<span class="asterisk">*</span></label>
                                <div class="col-sm-7">
                                	<select name="mfg_id" id="mfg_id" class="form-control">
                                		<option value="">Select Mfg name</option>
                                		<?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mfg_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
?>
                                		<option value="<?php echo $_smarty_tpl->tpl_vars['val']->value->id;?>
" <?php if ($_smarty_tpl->tpl_vars['mfg_id']->value==$_smarty_tpl->tpl_vars['val']->value->id) {?> selected="selected" <?php }?>><?php echo $_smarty_tpl->tpl_vars['val']->value->mfg_name;?>
</option>
                                		<?php } ?>
                                	</select>									
									</div>
								</div>
					</div>	
						<div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Model no<span class="asterisk">*</span></label>
                                <div class="col-sm-7">
									<input class="form-control" id="model_no" name="model_no" type="text" value="<?php echo $_smarty_tpl->tpl_vars['model_no']->value;?>
" autocomplete="off">
									
								</div>
							</div>
						</div>
						<div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Model Name<span class="asterisk">*</span></label>
                                <div class="col-sm-7">
									<input class="form-control" id="name" name="name" type="text" value="<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
" autocomplete="off">									
								</div>
							</div>
						</div>
						<div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Qty<span class="asterisk">*</span></label>
                                <div class="col-sm-7">
									<input class="form-control" id="qty" name="qty" type="text" value="<?php echo $_smarty_tpl->tpl_vars['qty']->value;?>
" autocomplete="off">									
								</div>
							</div>
						</div>

						<div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">price<span class="asterisk">*</span></label>
                                <div class="col-sm-7">
									<input class="form-control" id="price" name="price" type="text" value="<?php echo $_smarty_tpl->tpl_vars['price']->value;?>
" autocomplete="off">									
								</div>
							</div>
						</div>

						<div class="form-group">
                                <label class="col-sm-3 control-label"> Status <span class="asterisk">*</span> </label>
                                <div class="col-sm-9">
									<div class="rdio-theme col-md-3">
                                        <input id="is_active" type="radio" name="is_active" value="Active" 
										<?php if ($_smarty_tpl->tpl_vars['is_active']->value=="Active") {?> checked <?php } else { ?> checked <?php }?>>
                                        <label for="user_status"> Active </label>
                                    </div>
                                    <div class="rdio-theme col-md-3">
                                        <input id="user_status" type="radio" name="is_active" value="Inactive"
										<?php if ($_smarty_tpl->tpl_vars['is_active']->value=="Inactive") {?> checked <?php }?>>
                                        <label for="user_status"> Inactive </label>
                                    </div>
								</div>
							</div>	

						<div class="form-footer">
							<div class="col-sm-offset-3">
								
								<button type="submit" class="btn btn-theme" name="add" >
								Submit</button>
								<button type="reset" class="btn btn-theme" name="rest" >
								Reset</button>
								<a href="<?php echo base_url('Models');?>
" class="btn btn-danger mr-5">Cancel</a>
								
							</div>
						</div><!-- /.form-footer -->
					</form>
					
				</div>
			</div>      
		</div>
	</div><!-- /.row --> 
	
</div><!-- /.body-content -->
<!--/ End body content -->

<?php echo '<script'; ?>
>	
	(function($,W,D)
	{
		var JQUERY4U = {};
		
		JQUERY4U.UTIL =
		{
			setupFormValidation: function()
			{
				
				//form validation rules
				$("#user_submit_form").validate({
					rules:
					{
						mfg_id:
						{
							required:true,
						},
						model_no:
						{
							required:true,
						},
						name:
						{
							required:true,
						},
						qty:
						{
							required:true,
						},
						price:
						{
							required:true,
						},
						is_active:
						{
							required:true,
						}

					},
					messages: 
					{
						mfg_id:
						{
							required: "Please select mfg name",
						},
						model_no:
						{
							required: "Please enter model no",
						},
						name:
						{
							required: "Please enter model name",
						},
						qty:
						{
							required: "Please enter model qty",
						},
						price:
						{
							required: "Please enter model price",
						},
						is_active:
						{
							required:"Please check status",
						}

					},
					
					submitHandler: function(form) 
					{
						console.log(window.ajaxoptions);
						$(form).ajaxSubmit(window.ajaxoptions);
					}
				});
			}
		}
		
		//when the dom has loaded setup form validation rules
		$(D).ready(function($)
		{
			JQUERY4U.UTIL.setupFormValidation();
		});
		
	})(jQuery, window, document);
<?php echo '</script'; ?>
>
<?php }} ?>
