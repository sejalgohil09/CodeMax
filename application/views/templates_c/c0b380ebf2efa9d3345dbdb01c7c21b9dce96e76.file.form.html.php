<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-04-28 14:40:46
         compiled from "D:\xampp\htdocs\car\application\views\templates\contents\manufacturers\form.html" */ ?>
<?php /*%%SmartyHeaderCode:23285ae431835fdcd2-99360246%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c0b380ebf2efa9d3345dbdb01c7c21b9dce96e76' => 
    array (
      0 => 'D:\\xampp\\htdocs\\car\\application\\views\\templates\\contents\\manufacturers\\form.html',
      1 => 1524906643,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23285ae431835fdcd2-99360246',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ae43183830553_02145640',
  'variables' => 
  array (
    'id' => 0,
    'mfg_master' => 0,
    'val' => 0,
    'mfg_name' => 0,
    'is_active' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ae43183830553_02145640')) {function content_5ae43183830553_02145640($_smarty_tpl) {?><!-- Start page header -->

    <?php if ($_smarty_tpl->tpl_vars['id']->value!="new") {?>
    <?php $_smarty_tpl->tpl_vars["mfg_name"] = new Smarty_variable('', null, 0);?>
    <?php $_smarty_tpl->tpl_vars["is_active"] = new Smarty_variable('', null, 0);?>
    

    <?php  $_smarty_tpl->tpl_vars['val'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['val']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['mfg_master']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['val']->key => $_smarty_tpl->tpl_vars['val']->value) {
$_smarty_tpl->tpl_vars['val']->_loop = true;
?>
      <?php $_smarty_tpl->tpl_vars['mfg_name'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->mfg_name, null, 0);?>
      <?php $_smarty_tpl->tpl_vars['is_active'] = new Smarty_variable($_smarty_tpl->tpl_vars['val']->value->is_active, null, 0);?>     
    <?php } ?>
    <?php }?>
<div class="body-content animated fadeIn">
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel rounded shadow">
				<div class="panel-heading">
					<div class="pull-left">
						<h3 class="panel-title">Add New Manufacturers</h3>
					</div><!-- /.pull-left -->              
					<div class="clearfix"></div>
				</div><!-- /.panel-heading -->
				<div class="panel-body no-padding">
					<form class="form-horizontal" id="user_submit_form" role="form" method="POST"  name="user_submit_form" action="<?php echo base_url('Manufacturers/add_record');?>
"">
					
					<input type="hidden" class="form-control" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
					
						<div class="form-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Name<span class="asterisk">*</span></label>
                                <div class="col-sm-7">
									<input class="form-control" id="mfg_name" name="mfg_name" type="text" value="<?php echo $_smarty_tpl->tpl_vars['mfg_name']->value;?>
" autocomplete="off">									
								</div>
							</div>	

							<div class="form-group">
                                <label class="col-sm-3 control-label"> Status <span class="asterisk">*</span> </label>
                                <div class="col-sm-9">
									<div class="rdio-theme col-md-3">
                                        <input id="is_active" type="radio" name="is_active" value="Active" 
										<?php if ($_smarty_tpl->tpl_vars['is_active']->value=="Active") {?> checked <?php } else { ?> checked <?php }?>>
                                        <label for="user_status"> Active </label>
                                    </div>
                                    <div class="rdio-theme col-md-3">
                                        <input id="user_status" type="radio" name="is_active" value="Inactive"
										<?php if ($_smarty_tpl->tpl_vars['is_active']->value=="Inactive") {?> checked <?php }?>>
                                        <label for="user_status"> Inactive </label>
                                    </div>
								</div>
							</div>						
							
						</div>
						<div class="form-footer">
							<div class="col-sm-offset-3">
								
								<button type="submit" class="btn btn-theme" name="add" >
								Submit</button>
								<button type="reset" class="btn btn-theme" name="rest" >
								Reset</button>
								<a href="<?php echo base_url('Manufacturers');?>
" class="btn btn-danger mr-5">Cancel</a>
								
							</div>
						</div><!-- /.form-footer -->
					</form>
					
				</div>
			</div>      
		</div>
	</div><!-- /.row --> 
	
</div><!-- /.body-content -->
<!--/ End body content -->

<?php echo '<script'; ?>
>	
	(function($,W,D)
	{
		var JQUERY4U = {};
		
		JQUERY4U.UTIL =
		{
			setupFormValidation: function()
			{
				
				//form validation rules
				$("#user_submit_form").validate({
					rules:
					{
						mfg_name:
						{
							required:true,
						},
						is_active:
						{
							required:true,
						}

					},
					messages: 
					{
						mfg_name:
						{
							required: "Please Enter Name",
						},						
						is_active:
						{
							required:"Please check status",
						}

					},
					
					submitHandler: function(form) 
					{
						console.log(window.ajaxoptions);
						$(form).ajaxSubmit(window.ajaxoptions);
					}
				});
			}
		}
		
		//when the dom has loaded setup form validation rules
		$(D).ready(function($)
		{
			JQUERY4U.UTIL.setupFormValidation();
		});
		
	})(jQuery, window, document);
<?php echo '</script'; ?>
>
<?php }} ?>
