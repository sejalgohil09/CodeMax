<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-04-28 12:12:55
         compiled from "D:\xampp\htdocs\car\application\views\templates\partials\header.html" */ ?>
<?php /*%%SmartyHeaderCode:175625ae417b8c12231-92749580%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2026ef4daa5762d126a190e633ea9095149e6028' => 
    array (
      0 => 'D:\\xampp\\htdocs\\car\\application\\views\\templates\\partials\\header.html',
      1 => 1524897771,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175625ae417b8c12231-92749580',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ae417b8c6fe48_71402612',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ae417b8c6fe48_71402612')) {function content_5ae417b8c6fe48_71402612($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include 'D:\\xampp\\htdocs\\car\\application\\third_party\\smarty\\libs\\plugins\\modifier.capitalize.php';
?><header id="header">

<!-- Start header left -->
<div class="header-left">
    <!-- Start offcanvas left: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
    <div class="navbar-minimize-mobile left">
        <i class="fa fa-bars"></i>
    </div>
    <!--/ End offcanvas left -->

	
    <!-- Start offcanvas right: This menu will take position at the top of template header (mobile only). Make sure that only #header have the `position: relative`, or it may cause unwanted behavior -->
    <div class="navbar-minimize-mobile right">
        <i class="fa fa-cog"></i>
    </div>
    <!--/ End offcanvas right -->

	
    <!-- Start navbar header -->
    <div class="navbar-header">
		<div class="col-md-12">
			<!-- Start brand -->
			<a class="navbar-brand" href="<?php echo base_url('Dashboard');?>
">
				 <!-- <img class="logo" src="<?php echo base_url('assets/logo.png');?>
  -->
				<!-- " alt="brand logo" width="150px" />	 -->
				Mini Car Inventory 
			</a><!-- /.navbar-brand -->
			<!--/ End brand -->
		</div>
    </div><!-- /.navbar-header -->
    <!--/ End navbar header -->

    <div class="clearfix"></div>
</div><!-- /.header-left -->
<!--/ End header left -->

<!-- Start header right -->
<div class="header-right">
<!-- Start navbar toolbar -->
<div class="navbar navbar-toolbar">

<!-- Start left navigation -->
<ul class="nav navbar-nav navbar-left">

    <!-- Start sidebar shrink -->
    <li class="navbar-minimize">
         
        <a href="javascript:void(0);" title="Minimize sidebar">
            <i class="fa fa-bars"></i>
        </a>
   
    </li>
    <!--/ End sidebar shrink -->
    <li class="navbar-search" >
		
        <form class="navbar-form" id="date_time">        
            <?php echo '<script'; ?>
 type="text/javascript">window.onload = date_time('date_time');<?php echo '</script'; ?>
>
        </form>
    </li>    
    <!--/ End form search -->

</ul><!-- /.navbar-left -->
<!--/ End left navigation -->

<!-- Start right navigation -->
<ul class="nav navbar-nav navbar-right"><!-- /.nav navbar-nav navbar-right -->

<!-- Start profile -->
<!-- <li class="dropdown navbar-profile">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="meta">
                                  <span class="text">Welcome <?php echo smarty_modifier_capitalize($_SESSION['full_name']);?>

                                </span>
                                    <span class="caret"></span>
                                </span>
    </a> -->
    <!-- Start dropdown menu -->
   <!--  <ul class="dropdown-menu animated flipInX">
        <li class="dropdown-header">Account</li>
        <li><a href="<?php echo base_url('profile_master');?>
"><i class="fa fa-user"></i>View Profile</a></li>      
        <li><a href="<?php echo base_url('Change_Password');?>
"><i class="fa fa-key"></i>Change Password</a></li>  
     <li><a  href="javascript:void(0);" data-toggle="modal" data-target="#myModal" ><i class="fa fa-sign-out"></i>Logout</a></li>  
    </ul> -->
    <!--/ End dropdown menu -->
<!-- </li>/.dropdown navbar-profile -->
<!--/ End profile -->

<!-- Start settings --
<li class="navbar-setting pull-right">
    <a href="javascript:void(0);"><i class="fa fa-cog fa-spin"></i></a>
</li><!-- /.navbar-setting pull-right -->
<!--/ End settings -->
</ul><!-- /.navbar-right -->
</div><!-- /.navbar-toolbar -->
<!--/ End navbar toolbar -->
</div><!-- /.header-right -->
<!--/ End header left -->

</header> <!-- /#header -->
<?php }} ?>
