<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-04-30 12:03:23
         compiled from "D:\xampp\htdocs\car\application\views\templates\contents\models\list.html" */ ?>
<?php /*%%SmartyHeaderCode:288835ae43707a56376-27153849%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa3cd5064bc231baa6e0f7f470c4ecb248aa95f8' => 
    array (
      0 => 'D:\\xampp\\htdocs\\car\\application\\views\\templates\\contents\\models\\list.html',
      1 => 1525070000,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '288835ae43707a56376-27153849',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ae43707b409a2_30043481',
  'variables' => 
  array (
    'counter' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ae43707b409a2_30043481')) {function content_5ae43707b409a2_30043481($_smarty_tpl) {?><div class="body-content animated fadeIn">
        
            <!-- Start datatable using dom -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                   
                        <a href="<?php echo base_url('Models/form');?>
">
                          <button type="button" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span> Add Model </button>
                          </a>

                           <button class="btn btn-danger " onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i>  Bulk Delete</button>
                   
                        
                       
                    </div>                
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="clearfix"></div>
                <div class="panel-body double-scroll">
                    <table id="table" class="table table-striped table-bordered table-theme" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:80px;"><input type="checkbox" id="check-all" onclick="checkedall()"> &nbsp; Action</th>
                                <th class="text-center">Sr No.</th>                        
                                <th class="text-center">Mfg Name</th>
                                <th class="text-center">Model No</th>
                                <th class="text-center">Name</th>                               
                                <th class="text-center">Qty</th>
                                <th class="text-center">price</th>
                                <th class="text-center">Is Active</th>                       
                            </tr>
                        </thead>
                        <thead>
                           <tr>
                           <?php $_smarty_tpl->tpl_vars["counter"] = new Smarty_variable(2, null, 0);?>
                           
                            <th> &nbsp; </th>
                            <th> &nbsp; </th>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                             <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>                            

                             <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                             <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>
                            
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Action</th>
                                <th class="text-center">Sr No.</th>                        
                                <th class="text-center">Mfg Name</th>
                                <th class="text-center">Model No</th>
                                <th class="text-center">Name</th>                                
                                <th class="text-center">Qty</th>
                                <th class="text-center">price</th>
                                <th class="text-center">Is Active</th>                     
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.panel-body -->
            </div>

    </div><!-- /.row -->
    <?php echo '</script'; ?>
>
 <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('assets/jquery-ui.min.js');?>
"><?php echo '</script'; ?>
>  
<?php echo '<script'; ?>
 type="text/javascript" language="javascript" >
        $(document).ready(function(){
            var dataTable = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "columnDefs": [{ "orderable": false, "targets": 0 }],
                "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
                "ajax":{
                    url :"<?php echo base_url();?>
Models/ajax_list", // json datasource
                    type: "post",  // method  , by default get
                    error: function(){  // error handling
                        $(".table_error").html("");
                        $("#table").append('<tbody class="table_error"><tr><th colspan="3"> No Controller or Method Name Found</th></tr></tbody>');                     
                    },
                }
            });

                  //comman search             
            $('.form-control').keyup('keyup',function(){
                dataTable.search($(this).val()).draw();
            });            

            $('.search-input-text').on( 'keyup click', function(){   // for text boxes
                var i =$(this).attr('data-column');  // getting column index
                var v =$(this).val();  // getting search input value
                dataTable.columns(i).search(v).draw();
            } );

            $('.search-input-select').on( 'change', function (){   // for select box
                var i =$(this).attr('data-column');  
                var v =$(this).val();  
                dataTable.columns(i).search(v).draw();
            });
            
            
            
        });
        

 // check all
    $("#check-all").change(function(){
        $(".data-check").prop('checked', $(this).prop('checked'));
    });

    function bulk_delete()
    {
        var list_id = [];
        $(".data-check:checked").each(function() {
                list_id.push(this.value);
        });
        
        if(list_id.length > 0)
        {
            if(confirm('Are you sure delete this '+list_id.length+' data?'))
            {
                // console.log(list_id);
                $.ajax({
                    type: "POST",
                    data: "list_id="+list_id,
                    url: "<?php echo base_url();?>
Models/ajax_bulk_delete",
                    dataType: "JSON",
                    success: function(data)
                    {
                        if(data.status)
                        {
                            console.log(data);
                            reload_table();
                        }
                        else
                        {
                            console.log(data);
                            alert('Failed.');
                        }
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log(data);
                        alert('Error deleting data');
                    }
                });
            }
        }
        else
        {
            alert('no data selected');
        }
    }

function reload_table()
{
    alert('Delete Successfully');
    $('#table').DataTable().ajax.reload(null,false).draw(); //reload datatable ajax 
}
<?php echo '</script'; ?>
><?php }} ?>
