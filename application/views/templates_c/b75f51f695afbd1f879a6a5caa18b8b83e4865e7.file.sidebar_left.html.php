<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-04-28 12:36:38
         compiled from "D:\xampp\htdocs\car\application\views\templates\partials\sidebar_left.html" */ ?>
<?php /*%%SmartyHeaderCode:209395ae417b8d75a07-83304125%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b75f51f695afbd1f879a6a5caa18b8b83e4865e7' => 
    array (
      0 => 'D:\\xampp\\htdocs\\car\\application\\views\\templates\\partials\\sidebar_left.html',
      1 => 1524899193,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '209395ae417b8d75a07-83304125',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ae417b8e7b5c9_95474667',
  'variables' => 
  array (
    'profile' => 0,
    'active_dashboard' => 0,
    'active_mfg' => 0,
    'active' => 0,
    'menu' => 0,
    'mn' => 0,
    'data' => 0,
    'action_segment' => 0,
    'keys' => 0,
    'sub' => 0,
    'i' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ae417b8e7b5c9_95474667')) {function content_5ae417b8e7b5c9_95474667($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_capitalize')) include 'D:\\xampp\\htdocs\\car\\application\\third_party\\smarty\\libs\\plugins\\modifier.capitalize.php';
?><aside id="sidebar-left" class="sidebar-circle sidebar-light">
    <div class="sidebar-content">
        <div class="media">
            <a class="pull-left has-notif avatar" href="page-profile.html">
				<?php if (empty($_SESSION['photo'])) {?> 
					<?php $_smarty_tpl->tpl_vars["profile"] = new Smarty_variable("default.png", null, 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars["profile"] = new Smarty_variable($_SESSION['photo'], null, 0);?> 
				<?php }?>
                <img src="<?php echo base_url('uploads');?>
/<?php echo $_smarty_tpl->tpl_vars['profile']->value;?>
" width="50px" height="50px" alt="admin">
            </a>
            <div class="media-body">
			<h5 class="media-heading"><span class="text"> Hello, <?php echo smarty_modifier_capitalize($_SESSION['full_name']);?>
</span></h5>
             <small><?php echo $_SESSION['userlevelsname'];?>
</small>
            </div>
        </div>
    </div>
<ul class="sidebar-menu">
    <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_dashboard']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_dashboard']->value;?>
 <?php }?>">
        <a href="<?php echo base_url('Dashboard');?>
">
        <span class="icon"><i class="fa fa-home"></i></span> &nbsp;&nbsp;
        <span class="text">Dashboard</span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_dashboard']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
        </a>
    </li>
    <li class="<?php if (isset($_smarty_tpl->tpl_vars['active_mfg']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active_mfg']->value;?>
 <?php }?>">
        <a href="<?php echo base_url('Manufacturers');?>
">
        <span class="icon"><i class="fa fa-industry"></i></span> &nbsp;&nbsp;
        <span class="text">Manufacturers</span>
        <?php if (isset($_smarty_tpl->tpl_vars['active_mfg']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
        </a>
    </li>
    <li class="<?php if (isset($_smarty_tpl->tpl_vars['active']->value)) {?> <?php echo $_smarty_tpl->tpl_vars['active']->value;?>
 <?php }?>">
        <a href="<?php echo base_url('Models');?>
">
        <span class="icon"><i class="fa fa-car"></i></span> &nbsp;&nbsp;
        <span class="text">Models</span>
        <?php if (isset($_smarty_tpl->tpl_vars['active']->value)) {?> <?php echo '<span class="selected"></span>';?>
 <?php }?>
        </a>
    </li>
<!-- <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable(0, null, 0);?>

<?php  $_smarty_tpl->tpl_vars['mn'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['mn']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu']->value['level1']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['mn']->key => $_smarty_tpl->tpl_vars['mn']->value) {
$_smarty_tpl->tpl_vars['mn']->_loop = true;
?>
           <?php $_smarty_tpl->tpl_vars["keys"] = new Smarty_variable(explode(',',$_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['mn']->value-1]->menu_active), null, 0);?>
                   
            <li class="submenu <?php if (in_array($_smarty_tpl->tpl_vars['action_segment']->value,$_smarty_tpl->tpl_vars['keys']->value)) {?> active <?php }?> <?php if ($_smarty_tpl->tpl_vars['action_segment']->value==$_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['mn']->value-1]->link) {?> active <?php }?>" data-container="body" data-toggle="tooltip" data-placement="right" title="<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['mn']->value-1]->menu_caption;?>
">
            <?php if (array_key_exists($_smarty_tpl->tpl_vars['mn']->value,$_smarty_tpl->tpl_vars['menu']->value['level2'])) {?>
            <a href="javascript:void(0);">
                <?php } else { ?>
            <a href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['mn']->value-1]->link;?>
">
                <?php }?>
            <span class="icon"><i class="<?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['mn']->value-1]->class_name;?>
"></i></span> &nbsp;&nbsp;
            <span class="text"><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['mn']->value-1]->menu_caption;?>
</span>           
            

            <?php if (array_key_exists($_smarty_tpl->tpl_vars['mn']->value,$_smarty_tpl->tpl_vars['menu']->value['level2'])) {?>
            <span class="arrow"></span>
            </a>
                <ul>
             
                <?php $_smarty_tpl->tpl_vars['k'] = new Smarty_variable(0, null, 0);?>
                <?php  $_smarty_tpl->tpl_vars['sub'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['sub']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['menu']->value['level2'][$_smarty_tpl->tpl_vars['mn']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['sub']->key => $_smarty_tpl->tpl_vars['sub']->value) {
$_smarty_tpl->tpl_vars['sub']->_loop = true;
?>
               
                <?php $_smarty_tpl->tpl_vars['k'] = new Smarty_variable($_smarty_tpl->tpl_vars['sub']->value, null, 0);?>
               
                    <li class="submenu <?php if ($_smarty_tpl->tpl_vars['action_segment']->value==$_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['sub']->value-1]->link) {?> active <?php }?>" >
                    <a href="<?php echo base_url();
echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['sub']->value-1]->link;?>
">
                    <span class="text"><?php echo $_smarty_tpl->tpl_vars['data']->value[$_smarty_tpl->tpl_vars['sub']->value-1]->menu_caption;?>
</span>               
                    </a>          
                    </li> 
               
                <?php } ?>     
                </ul>
            <?php } else { ?>
        </a>
            <?php }?>
           
           
            </li>
             
            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_variable($_smarty_tpl->tpl_vars['i']->value+1, null, 0);?>
           
<?php } ?> -->

    </ul>


</ul><!-- /.sidebar-menu -->
<!--/ End left navigation - menu -->

<!-- Start left navigation - footer -->
<!-- /.sidebar-footer -->
<!--/ End left navigation - footer -->  

<div class="sidebar-footer hidden-xs hidden-sm hidden-md">
    <a id="logs" class="pull-left" href="<?php echo base_url('logs_master');?>
" data-toggle="tooltip" data-placement="top" data-title="Logs"><i class="fa fa-cog" data-container="body" data-toggle="tooltip" data-placement="right" title="Logs"></i></a>
    <a id="fullscreen" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Fullscreen"><i class="fa fa-desktop"data-container="body" data-toggle="tooltip" data-placement="right" title="Full Screen"></i></a>
  <!--   <a id="lock-screen" data-url="account/lock_screen1" class="pull-left" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" data-title="Lock Screen"><i class="fa fa-lock"></i></a> -->
    <a data-placement="top" data-target="#myModal" class="pull-left" href="javascript:void(0);" data-toggle="modal" data-title="Lock Screen"><i class="fa fa-power-off" data-container="body" data-toggle="tooltip" data-placement="right" title="Logout"></i></a>
</div>
</aside>

    <?php }} ?>
