<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-04-28 12:34:49
         compiled from "D:\xampp\htdocs\car\application\views\templates\contents\profiles\list.html" */ ?>
<?php /*%%SmartyHeaderCode:313185ae41d1176aca1-00352436%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd04c44557d787d460a74e67d4fc681d2e51c9958' => 
    array (
      0 => 'D:\\xampp\\htdocs\\car\\application\\views\\templates\\contents\\profiles\\list.html',
      1 => 1512543316,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '313185ae41d1176aca1-00352436',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'counter' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ae41d11897978_70643458',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ae41d11897978_70643458')) {function content_5ae41d11897978_70643458($_smarty_tpl) {?>

<?php echo generateBreadcrumb();?>

<div class="body-content animated fadeIn">
        
            <!-- Start datatable using dom -->
            <div class="panel rounded shadow">
                <div class="panel-heading">
                    <div class="pull-left">
                    <?php if ($_SESSION['user_level']<3) {?>
                        <a href="<?php echo base_url('Profiles/form');?>
">
                          <button type="button" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span> Add New Profile</button>
                          </a>
                    <?php }?>
                        <?php if ($_SESSION['user_level']==1) {?>  
                        <button class="btn btn-danger " onclick="bulk_delete()"><i class="glyphicon glyphicon-trash"></i>  Bulk Delete</button>
                        <?php }?>
                        <?php if ($_SESSION['user_level']==1) {?>
                         <a href="<?php echo base_url('Profiles/export');?>
"><button  type="button" class="btn btn-primary">Export</button></a>
                         <a href="<?php echo base_url('Profiles/import');?>
"><button  type="button" class="btn btn-primary">Import</button></a>
                         <?php }?>
                       
                    </div>                
                    <div class="clearfix"></div>
                </div><!-- /.panel-heading -->
                <div class="clearfix"></div>
                <div class="panel-body double-scroll">
                    <table id="table" class="table table-striped table-bordered table-theme" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:80px;"><input type="checkbox" id="check-all" onclick="checkedall()"> &nbsp; Action</th>
                                <th class="text-center">Sr No.</th>                        
                                <th class="text-center">Profile Name</th>
                                <th class="text-center">Test Code</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Units</th>
                                <th class="text-center">Method</th>
                                <th class="text-center">All Tests</th>
                                <th class="text-center">In Package</th>
                                <th class="text-center">Comments</th>
                                <th class="text-center">Attachment</th>
                            </tr>
                        </thead>
                        <thead>
                           <tr>
                           <?php $_smarty_tpl->tpl_vars["counter"] = new Smarty_variable(2, null, 0);?>
                           
                            <th> &nbsp; </th>
                            <th> &nbsp; </th>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                            <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>

                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>
                            <?php $_smarty_tpl->tpl_vars['counter'] = new Smarty_variable($_smarty_tpl->tpl_vars['counter']->value+1, null, 0);?>
                                <th style="text-align: center;"><input type="text" data-column="<?php echo $_smarty_tpl->tpl_vars['counter']->value;?>
"  class="search-input-text"></th>
                           </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Action</th>
                                <th class="text-center">Sr No.</th>                        
                                <th class="text-center">Profile Name</th>
                                <th class="text-center">Test Code</th>
                                <th class="text-center">Description</th>
                                <th class="text-center">Units</th>
                                <th class="text-center">Method</th>
                                <th class="text-center">All Tests</th>
                                <th class="text-center">In Package</th>
                                <th class="text-center">Comments</th>                      
                                <th class="text-center">Attachment</th>                      
                            </tr>
                        </tfoot>
                    </table>
                </div><!-- /.panel-body -->
            </div>

    </div><!-- /.row -->
    <?php echo '</script'; ?>
>
 <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('assets/custom_js/jquery-ui.min.js');?>
"><?php echo '</script'; ?>
>  
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo base_url('assets/custom_js/jquery.doubleScroll.js');?>
"><?php echo '</script'; ?>
> 
        
          <?php echo '<script'; ?>
 type="text/javascript">
            $(document).ready(function(){
               $('.double-scroll').doubleScroll();
            });
   <?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" language="javascript" >
        $(document).ready(function(){
            var dataTable = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "columnDefs": [{ "orderable": false, "targets": 0 }],
                "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
                "ajax":{
                    url :"<?php echo base_url();?>
Profiles/ajax_list", // json datasource
                    type: "post",  // method  , by default get
                    error: function(){  // error handling
                        $(".table_error").html("");
                        $("#table").append('<tbody class="table_error"><tr><th colspan="3"> No Controller or Method Name Found</th></tr></tbody>');                     
                    },
                }
            });

                  //comman search             
            $('.form-control').keyup('keyup',function(){
                dataTable.search($(this).val()).draw();
            });            

            $('.search-input-text').on( 'keyup click', function(){   // for text boxes
                var i =$(this).attr('data-column');  // getting column index
                var v =$(this).val();  // getting search input value
                dataTable.columns(i).search(v).draw();
            } );

            $('.search-input-select').on( 'change', function (){   // for select box
                var i =$(this).attr('data-column');  
                var v =$(this).val();  
                dataTable.columns(i).search(v).draw();
            });
            
            
            
        });
        

 // check all
    $("#check-all").change(function(){
        $(".data-check").prop('checked', $(this).prop('checked'));
    });






    function bulk_delete()
    {
        var list_id = [];
        $(".data-check:checked").each(function() {
                list_id.push(this.value);
        });
        
        if(list_id.length > 0)
        {
            if(confirm('Are you sure delete this '+list_id.length+' data?'))
            {
                // console.log(list_id);
                $.ajax({
                    type: "POST",
                    data: "list_id="+list_id,
                    url: "<?php echo base_url();?>
Profiles/ajax_bulk_delete",
                    dataType: "JSON",
                    success: function(data)
                    {
                        if(data.status)
                        {
                            console.log(data);
                            reload_table();
                        }
                        else
                        {
                            console.log(data);
                            alert('Failed.');
                        }
                        
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log(data);
                        alert('Error deleting data');
                    }
                });
            }
        }
        else
        {
            alert('no data selected');
        }
    }

function reload_table()
{
    alert('Delete Successfully');
    $('#table').DataTable().ajax.reload(null,false).draw(); //reload datatable ajax 
}
<?php echo '</script'; ?>
><?php }} ?>
