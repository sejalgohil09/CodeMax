<?php /* Smarty version Smarty-3.1.21-dev, created on 2018-04-28 13:43:12
         compiled from "D:\xampp\htdocs\car\application\views\templates\contents\layout.html" */ ?>
<?php /*%%SmartyHeaderCode:54455ae417b87821a7-16666002%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '565d2fe55a0e88d24c36ebc069101e95771b0b1b' => 
    array (
      0 => 'D:\\xampp\\htdocs\\car\\application\\views\\templates\\contents\\layout.html',
      1 => 1524903169,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '54455ae417b87821a7-16666002',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ae417b8b85817_86946446',
  'variables' => 
  array (
    'title' => 0,
    'list_css_plugin' => 0,
    'list_css' => 0,
    'body' => 0,
    'list_js_cdn' => 0,
    'list_js' => 0,
    'list_js_plugin' => 0,
    'list_js_commercial' => 0,
    'list_js_page' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ae417b8b85817_86946446')) {function content_5ae417b8b85817_86946446($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en"> 
	<head>
		<!-- START @META SECTION -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="Trusto Healthcare">
		<meta name="keywords" content="Trusto Healthcare">
		<meta name="author" content="Trusto Healthcare">
		<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 | Trusto Healthcare</title>
		<!--/ END META SECTION -->
		<link rel="shortcut icon" type="image/icon" href="<?php echo base_url('assets/favicon.ico');?>
">
		
		
		<!-- START @PAGE LEVEL STYLES -->
		<!-- CSS plugins -->
		<?php if (isset($_smarty_tpl->tpl_vars['list_css_plugin']->value)) {?>
        <?php  $_smarty_tpl->tpl_vars['list_css'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list_css']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_css_plugin']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list_css']->key => $_smarty_tpl->tpl_vars['list_css']->value) {
$_smarty_tpl->tpl_vars['list_css']->_loop = true;
?>
		<link href="<?php echo base_url(('assets/global/plugins/bower_components/').($_smarty_tpl->tpl_vars['list_css']->value));?>
" rel="stylesheet">
        <?php } ?>
		<?php }?>
		
		<?php echo '<script'; ?>
 src="<?php echo base_url('assets/global/plugins/bower_components/jquery/dist/jquery-2.1.1.min.js');?>
"><?php echo '</script'; ?>
>
		<?php echo '<script'; ?>
 src="<?php echo base_url();?>
assets/global/plugins/bower_components/admin/js/apps.js"><?php echo '</script'; ?>
>
		

		
		<!---Close Script and CSS ----------------------->
		
		<!---------auto load time date on header------------>
		
        <?php echo '<script'; ?>
 src="<?php echo base_url('assets/admin/js/date_time.js');?>
"><?php echo '</script'; ?>
>
		<link href="<?php echo base_url('assets/admin/css/date_time.css');?>
" rel="stylesheet">
                 
	</head>	
	<body>
		
		
		<!-- START @WRAPPER -->
		<section id="wrapper">
			
			<?php echo $_smarty_tpl->getSubTemplate ('partials/header.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			<?php echo $_smarty_tpl->getSubTemplate ('partials/sidebar_left.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			
			<section id="page-content">
			
				<?php echo $_smarty_tpl->getSubTemplate ($_smarty_tpl->tpl_vars['body']->value, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

				<?php echo $_smarty_tpl->getSubTemplate ('partials/footer.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

				
			</section>
			
			<?php echo $_smarty_tpl->getSubTemplate ('partials/sidebar_right.html', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

			
		</section>
		
		<div id="back-top" class="animated pulse circle">
			<i class="fa fa-angle-up"></i>
		</div>
		
		
		
		<!-- START @PAGE LEVEL PLUGINS -->
		<!-- CDN -->
		<?php if (isset($_smarty_tpl->tpl_vars['list_js_cdn']->value)) {?>
		<?php  $_smarty_tpl->tpl_vars['list_js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list_js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_js_cdn']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list_js']->key => $_smarty_tpl->tpl_vars['list_js']->value) {
$_smarty_tpl->tpl_vars['list_js']->_loop = true;
?>
		<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['list_js']->value;?>
"><?php echo '</script'; ?>
>
		<?php } ?>
		<?php }?>
		
		<?php if (isset($_smarty_tpl->tpl_vars['list_js_plugin']->value)) {?>
		<?php  $_smarty_tpl->tpl_vars['list_js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list_js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_js_plugin']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list_js']->key => $_smarty_tpl->tpl_vars['list_js']->value) {
$_smarty_tpl->tpl_vars['list_js']->_loop = true;
?>
        <?php echo '<script'; ?>
 src="<?php echo base_url(('assets/global/plugins/bower_components/').($_smarty_tpl->tpl_vars['list_js']->value));?>
"><?php echo '</script'; ?>
>
		<?php } ?>
		<?php }?>
		
		<?php if (isset($_smarty_tpl->tpl_vars['list_js_commercial']->value)) {?>
		<?php  $_smarty_tpl->tpl_vars['list_js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list_js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_js_commercial']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list_js']->key => $_smarty_tpl->tpl_vars['list_js']->value) {
$_smarty_tpl->tpl_vars['list_js']->_loop = true;
?>
		<?php echo '<script'; ?>
 src="<?php echo base_url(('assets/commercial/plugins/').($_smarty_tpl->tpl_vars['list_js']->value));?>
"><?php echo '</script'; ?>
>
		<?php } ?>
		<?php }?>
		<!--/ END PAGE LEVEL PLUGINS -->
		
		<!-- START @PAGE LEVEL SCRIPTS -->
		<?php echo '<script'; ?>
 src="<?php echo base_url('assets/admin/js/apps.js');?>
"><?php echo '</script'; ?>
>
		<?php if (isset($_smarty_tpl->tpl_vars['list_js_page']->value)) {?>
		<?php  $_smarty_tpl->tpl_vars['list_js'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['list_js']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['list_js_page']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['list_js']->key => $_smarty_tpl->tpl_vars['list_js']->value) {
$_smarty_tpl->tpl_vars['list_js']->_loop = true;
?>
        <?php echo '<script'; ?>
 src="<?php echo base_url(('assets/admin/js/pages/').($_smarty_tpl->tpl_vars['list_js']->value));?>
"><?php echo '</script'; ?>
>
		<?php } ?>
		<?php }?>
		
		
		
		<!-- START GOOGLE ANALYTICS of SALES MULTIPLIER -->
		<?php echo '<script'; ?>
>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-86914016-1', 'auto');
		  ga('send', 'pageview');

		<?php echo '</script'; ?>
>
		<!--/ END GOOGLE ANALYTICS -->
		
		
		
		
		
		
	</html>
	 
	
	<?php }} ?>
