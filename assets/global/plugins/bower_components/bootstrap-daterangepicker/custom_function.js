$(function() {

  $('input[name="daterange"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
      	  format: 'DD-MM-YYYY',
          cancelLabel: 'Clear'
      }
  });


  $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
    
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });


//date picker 1-------------------------------------------
  $('input[name="daterange_datetow"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          format: 'DD-MM-YYYY',
          cancelLabel: 'Clear'
      }
  });


  $('input[name="daterange_datetow"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('input[name="daterange_datetow"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
//date picker 1-------------------------------------------

  $('input[name="daterange_datethree"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          format: 'DD-MM-YYYY',
          cancelLabel: 'Clear'
      }
  });
//date picker 2
  $('input[name="daterange_datethree"]').on('apply.daterange_datethree', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
  });

  $('input[name="daterange_datethree"]').on('cancel.daterange_datethree', function(ev, picker) {
      $(this).val('');
  });
});