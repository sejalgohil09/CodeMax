  $(function() {
  //wait till the page is fully loaded before loading table
  //dataTableSearch() is optional.  It is a jQuery plugin that looks for input fields in the thead to bind to the table searching
  $("#sampleOrderTable").dataTable({
     processing: true,
        serverSide: true,
        ajax: {
            "url": "{base_url()}Fixed_task/dataTable",
            "type": "POST"
        },
        columns: [
          { data: "f.id" },
          { data: "d.department_name" },
          { data: "f.task_name" },
          { data: "f.action_day" },
        ]
  }).dataTableSearch(500);
});
