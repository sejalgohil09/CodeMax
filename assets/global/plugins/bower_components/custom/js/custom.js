// prepare the form when the DOM is ready 
$(document).ready(function() { 
	
	
    // bind form using 'ajaxForm' 
	window.ajaxoptions = 
	{
		
		beforeSubmit: function(){
			
			},
        success:       function(data){
				
				var IS_JSON = true;
					try{
						
							data = $.parseJSON(data);
						}
					catch(err){
						
							IS_JSON = false;
						}
					
					
				if(IS_JSON)
				{
													
						if(data.success)
						{

							$.notify(data.message,"success");
							console.log(data);
							if(data.redirect){
								window.location.href = data.redirect;
							}
							if(data.reload){
								location.reload();
							}
						}
						else
						{
							$.notify(data.message,"error");
						}
				}
				else
				{
					$.notify("Data is not JSON format !!!","error");
				}
			},
 
        // other available options: 
        //url:       url         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
        //clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 
 
        // $.ajax options can be used here too, for example: 
        //timeout:   3000 
    }; 
}); 